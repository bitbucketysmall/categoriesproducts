<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $categories = factory(Category::class, 80)->create()
                ->each(function ($category) {
            $categChildren = factory(Category::class, 20)->make()
                    ->each(function ($categChild) use ($category) {
                $category->appendNode($categChild);
//                $categGrandChildren = factory(Category::class, 25)->make()
//                        ->each(function ($categGrandChild) use ($categChild) {
//                    $categChild->appendNode($categGrandChild);
//                    $categGrandGrandChildren = factory(Category::class, 45)->make()
//                            ->each(function ($categGrandGrandChild) use ($categGrandChild) {
//                        $categGrandChild->appendNode($categGrandGrandChild);
//                    });
//                });
            });
        });
    }
}
