<?php

namespace App;

class Taverse {

    protected $traverseResult;

    public function __construct() {
        $this->traverseResult = '';
    }

    public function traverse($categories) {

        foreach ($categories as $category) {
            $this->traverseResult .= "<li>";
            //view('categories.index')->render();
            $this->traverseResult .= $category->name;
            if (count($category->children)>0) {
              $this->setChildHtml($category->children);  
            }
            $this->traverseResult .= "</li>";
        }
    }

    protected function setChildHtml($childCategory) {
        $this->traverseResult .= "<ul>";
        $this->traverse($childCategory);
        $this->traverseResult .= "</ul>";
    }

    public function getTraverseResult() {

        if ($this->traverseResult != '')
            return $this->traverseResult;
        else {
            return 'dddd';
        }
    }

}
