<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Taverse;

class CategoryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Taverse $traverse) {

//        debug_print_backtrace(2);
//        $node = Category::create([
//                    'name' => 'Women',
//                    'alias' => 'women',
//                    'children' => [
//                        [
//                            'name' => 'Dresses',
//                            'alias' => 'dresses',
//                            'children' => [
//                                ['name' => 'Cocktails','alias' => 'cocktails',],
//                            ],
//                        ],
//                    ],
//        ]);
        //include(app_path() . '\Functions\functions.php');

        $categories = Category::withDepth()->where('parent_id', '=', null)->get();

        return view('categories.index', [
            'categories' => $categories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {

        $parentCategoryId = $request->input('clickedCategory');
        $parentCategory = Category::find($parentCategoryId);

        return view('categories.create', [
            'parentCategory' => $parentCategory,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        Category::create($request->all());

        return redirect()->action('CategoryController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category) {

        $categories = Category::withDepth()->where('parent_id', '=', null)->get();
        $requestedCategory = Category::withDepth()->find($category->id);
        $ancestorsIds = $category->ancestors()->pluck('id');

        if (count($ancestorsIds) > 0) {
            $parentOfRequestedCategoryCollection = Category::withDepth()->descendantsAndSelf($ancestorsIds[0])->toTree();
        } else {
            $parentOfRequestedCategoryCollection = Category::withDepth()->descendantsAndSelf($category->id)->toTree();
        }
        
        $parentOfRequestedCategory = $parentOfRequestedCategoryCollection[0];


        return view('categories.show', [
            'categories' => $categories,
            'parentOfRequestedCategory' => $parentOfRequestedCategory,
            'requestedCategory' => $requestedCategory,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category) {
        
        return view('categories.edit', [
            'category' => $category,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category) {
        $category->delete();
        return redirect()->back();
    }

}
