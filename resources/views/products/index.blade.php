@extends('layouts.app')

@section('content')

<!-- Current categories -->
<div class="panel panel-default">

    @if(count($products) > 0)
    <div class="productsContainer">
    @foreach($products as $iterator=> $product)

    @if( 0 == $iterator % 5 ) 
    </div>
    <div class="productsContainer">
        <div class="productsItem">
            <div>{{$product->image}}</div> 
            <div>{{$product->name}}</div> 
            <div>{{$product->description}}</div>
            <div>{{$product->price}}</div>
            <div>{{$product->category->name}}</div>
        </div>    
    @else
        <div class="productsItem">
            <div>{{$product->image}}</div> 
            <div>{{$product->name}}</div> 
            <div>{{$product->description}}</div>
            <div>{{$product->price}}</div>
            <div>{{$product->category->name}}</div>
        </div>
    @endif        
    @endforeach
    </div>
    @endif

    </div>

    <div clas    s="panel-body"> 
        <!-- To New Category Form -->
        <form action="{{ route('categories.products.create', [$category]) }}" 
              method="GET" class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus">New Product</i>
                    </button>
                </div>
            </div>
        </form>
    </div>

    @endsection




