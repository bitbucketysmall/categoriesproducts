@extends('layouts.app')

@section('content')

<div class="panel-body">

    <form action="{{route('categories.products.store', [$category])}}" method="POST" class="form-horizontal">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="product-name" class="col-sm-3 control-label">
                product
            </label>
            <div class="col-sm-6">
                <input type="text" name="name" id="product-name" class="form-control">
            </div>
            <label for="product-alias" class="col-sm-3 control-label">
                alias
            </label>
            <div class="col-sm-6">
                <input type="text" name="alias" id="product-alias" class="form-control">
            </div> 
            <label for="product-price" class="col-sm-3 control-label">
                price
            </label>
            <div class="col-sm-6">
                <input type="text" name="price" id="product-price" class="form-control">
            </div> 
            <label for="product-description" class="col-sm-3 control-label">
                description
            </label>
            <div class="col-sm-6">
                <textarea name="description" id="product-alias" class="form-control"></textarea>
            </div>    
            <label for="product-parent" class="col-sm-3 control-label">
                category 
            </label>
            <div class="col-sm-6">            
                <input id="product-parent" class="form-control" disabled="" value="{{$category->name}}">
                <input hidden="" name="category_id" value="{{$category->id}}">  
            </div>            
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-plus">Add product</i>
                </button>
            </div>
        </div>
    </form>
</div>
@endsection

