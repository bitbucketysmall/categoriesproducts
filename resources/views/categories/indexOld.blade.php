@extends('layouts.app')

@section('content')

<!-- Current categories -->
<div class="panel panel-default">

    @if($categories)
    <ul>
        @each('categories.recursive', $categories, 'category')
    </ul>
    @endif

</div>

<div class="panel-body"> 
    <!-- To New Category Form -->
    <form action="{{ url('categories/create') }}" method="GET" class="form-horizontal">
        {{ csrf_field() }}

        <!-- Go To create Button -->
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-plus"></i> New category
                </button>
            </div>
        </div>
    </form>
</div>

@endsection




