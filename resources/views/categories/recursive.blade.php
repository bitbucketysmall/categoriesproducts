<li style="margin-bottom: 20px;">    
    <a  href="{{ route('categories.show', [$category]) }}">{{$category->name}}</a>
    <form style="display: inline;" action="{{ url('categories/create') }}" method="GET" class="form-horizontal">
        {{ csrf_field() }}
        <input hidden="" name="clickedCategory" value="{{$category->id}}">
        <button  type="submit" class="btn btn-default">
            <i class="fa fa-plus">+</i>
        </button>
    </form>
    <form style="display: inline;" action="{{ route('categories.destroy', [$category]) }}" method="POST" class="form-horizontal">
        @method('DELETE')
        {{ csrf_field() }}
        <button  type="submit" class="btn btn-default">
            <i class="fa fa-plus">delete</i>
        </button>
    </form>
    <form style="display: inline;" action="{{ route('categories.edit', [$category]) }}" method="GET" class="form-horizontal">              
        <button  type="submit" class="btn btn-default">
            <i class="fa fa-plus">edit</i>
        </button>
    </form>
    <form style="display: inline;" action="{{url('categories/'. $category->id. '/products') }}"  method="GET" class="form-horizontal">              
        <button  type="submit" class="btn btn-default">
            <i class="fa fa-plus">see products</i>
        </button>
    </form>
    @if (count($category->children)>0)
    @foreach ($category->children as $category)
    <ul style="margin-bottom: 20px; margin-top: 20px;">     
        @if($category->depth <= $requestedCategory->depth+1)
            @include('categories.recursive', ['category' => $category,'requestedCategory' => $requestedCategory])
        @endif 
    </ul>   
    @endforeach
    @endif 
</li>

