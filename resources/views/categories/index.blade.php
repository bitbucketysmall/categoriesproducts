@extends('layouts.app')

@section('content')

@if(count($categories) > 0)
<!-- Current categories -->
<div class="panel panel-default">  
    <ul>
        @foreach($categories as $category)  
        <li  style="margin-bottom: 20px;">      
            <a  href="{{ route('categories.show', [$category]) }}">{{$category->name}}</a>            
            <form style="display: inline;" action="{{ url('categories/create') }}" method="GET" class="form-horizontal">
                {{ csrf_field() }}
                <input hidden="" name="clickedCategory" value="{{$category->id}}">
                <button  type="submit" class="btn btn-default">
                    <i class="fa fa-plus">+</i>
                </button>
            </form>
            <form style="display: inline;" action="{{ route('categories.destroy', [$category]) }}" method="POST" class="form-horizontal">
                @method('DELETE')
                {{ csrf_field() }}
                <button  type="submit" class="btn btn-default">
                    <i class="fa fa-plus">delete</i>
                </button>
            </form>
            <form style="display: inline;" action="{{ route('categories.edit', [$category]) }}" method="GET" class="form-horizontal">              
                <button  type="submit" class="btn btn-default">
                    <i class="fa fa-plus">edit</i>
                </button>
            </form>
            <form style="display: inline;" action="{{url('categories/'. $category->id. '/products') }}"  method="GET" class="form-horizontal">              
                <button  type="submit" class="btn btn-default">
                    <i class="fa fa-plus">see products</i>
                </button>
            </form>
        </li>     
        @endforeach
    </ul>
</div>
@endif

<div class="panel-body"> 
    <!-- To New Category Form -->
    <form action="{{ url('categories/create') }}" method="GET" class="form-horizontal">
        {{ csrf_field() }}

        <!-- Go To create Button -->
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-plus"></i> New category
                </button>
            </div>
        </div>
    </form>
</div>

@endsection




