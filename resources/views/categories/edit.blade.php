@extends('layouts.app')
@section('content')

<div class="panel-body">

    <form action="{{ route('categories.update', [$category]) }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}
        @method('PUT')
        <div class="form-group">
            <label for="category-name" class="col-sm-3 control-label">
                category
            </label>
            <div class="col-sm-6">
                <input type="text" name="name" value="{{ $category->name }}" id="category-name" class="form-control">
            </div>
            <label for="category-alias" class="col-sm-3 control-label">
                alias
            </label>
            <div class="col-sm-6">
                <input type="text" name="alias" value="{{ $category->alias }}" id="category-alias" class="form-control">
            </div>
            <label for="category-parent" class="col-sm-3 control-label">
                parent 
            </label>
                     
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-plus">Add category</i>
                </button>
            </div>
        </div>
    </form>
</div>
@endsection
