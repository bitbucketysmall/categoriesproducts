
<li>  
    <div class="user_inside_mark_chart">
        <span class="submit_link">{{$category->name}}</span>
        <!-- To New Category Form -->
        <form  action="{{url('categories/'. $category->id. '/products') }}" method="GET" class="form-horizontal see_category_form">            
        </form>
    </div>
    @if (count($category->children)>0)
    <ul>
        @each('categories.recursive', $category->children, 'category')
    </ul>
    @endif
</li>


