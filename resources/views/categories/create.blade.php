@extends('layouts.app')

@section('content')

<!-- Bootstrap Boilerplate... -->
<div class="panel-body">
    <!-- Display Validation Errors -->
    @include('common.errors')
    <!-- New Subject Form -->
    <form action="{{ url('categories') }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="category-name" class="col-sm-3 control-label">
                category
            </label>
            <div class="col-sm-6">
                <input type="text" name="name" id="category-name" class="form-control">
            </div>
            <label for="category-alias" class="col-sm-3 control-label">
                alias
            </label>
            <div class="col-sm-6">
                <input type="text" name="alias" id="category-alias" class="form-control">
            </div>
            @if (request("clickedCategory"))
            <label for="category-parent" class="col-sm-3 control-label">
                selected 
            </label>
            <div class="col-sm-6">            
                <input id="category-parent" class="form-control" disabled="" value="{{$parentCategory->name}}">
                <input hidden="" name="parent_id" value="{{$parentCategory->id}}">  
            </div>            
            @endif
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-plus">Add category</i>
                </button>
            </div>
        </div>
    </form>
</div>
@endsection

